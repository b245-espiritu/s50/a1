import { useContext, useEffect } from "react";
import { Navigate } from "react-router-dom"
import UserContext from "../userContext";

export default function Logout(){

    const {unSetUser,setUser } = useContext(UserContext)
   
 
    useEffect(()=>{
        unSetUser();
        setUser(localStorage.getItem("token"))

    })
    return(
       <Navigate to = "/login"/>
    )
}