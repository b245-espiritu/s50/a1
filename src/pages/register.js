import { useContext, useEffect, useState } from 'react';
import Button from 'react-bootstrap/Button';
import Form from 'react-bootstrap/Form';
import { Navigate, useNavigate } from 'react-router-dom';
import Swal from 'sweetalert2';
import UserContext from '../userContext';

export default function Registration(){

    const [ email, setEmail] = useState('');
    const [ password, setPassword] = useState('')
    const [ confirmPassword, setConfirmPassword] = useState('')

    const [ firstName, setFirstName] = useState('')
    const [ lastName, setLastName] = useState('')
    const [ mobileNo, setMobileNo] = useState('')


    const [btnIsActive, setBtnIsActive] = useState(false);

    const navigate = useNavigate();

       useEffect( ()=>{
        if(email !=="" && password !== "" && confirmPassword !=="" && firstName !=="" && lastName !=="" && mobileNo !==""){
            if(password === confirmPassword){
                setBtnIsActive(true)
            }
            else{
                setBtnIsActive(false)
            }
        }
        else{
            setBtnIsActive(false)
        }

       

    },[email, password, confirmPassword, firstName, lastName, mobileNo])

    const {user} =useContext(UserContext);

    function register(event){
        event.preventDefault();


        fetch(`${process.env.REACT_APP_API_URL}/user/register`, 
            {
                method: 'POST',
                headers: {
                    'Content-Type' : 'application/json'
                },
                body: JSON.stringify(
                    {   
                        firstName: firstName,
                        lastName: lastName,
                        mobileNo: mobileNo,
                        email: email,
                        password: password
                    }
                )
            }
        )

        .then(result => result.json())
        .then(data => {
            if(data){
                Swal.fire({
                    title: "Successfully Registered!",
                    icon:'success',
                    text: 'Thank you for registration!'
                })

                navigate('/login');
            }
            else{
                Swal.fire({
                    title:"This email is already exist!",
                    icon:'error',
                    text:'Please try with another email!'
                })
            }
        })
       

        // alert('Congratulation you are now registered on our website!');

        

        // setUser(localStorage.setItem("email", email));
        // setUser(localStorage.getItem("email", email))

        setEmail('');
        setPassword('');
        setConfirmPassword('');

        setFirstName('');
        setLastName('');
        setMobileNo('');
    }

    return(
        user ? <Navigate to = '*'/> :
        <>
            <h1 className='text-center mt-5'>Register</h1>
            <Form onSubmit={event=> register(event)} className='mt-5'>

            <Form.Group className="mb-3" controlId="formBasicFirstName">
                    <Form.Label>First Name</Form.Label>

                    <Form.Control 
                        type="text" 
                        placeholder="First Name" 
                        value={firstName}
                        onChange={event => setFirstName(event.target.value)}
                        required
                    />

                </Form.Group>

                <Form.Group className="mb-3" controlId="formBasicLastName">
                    <Form.Label>Last Name</Form.Label>

                    <Form.Control 
                        type="text" 
                        placeholder="First Name" 
                        value={lastName}
                        onChange={event => setLastName(event.target.value)}
                        required
                    />

                </Form.Group>

                
                <Form.Group className="mb-3" controlId="formBasicMobileNo">
                    <Form.Label>Mobile Number</Form.Label>

                    <Form.Control 
                        type="text" 
                        placeholder="Mobile Number" 
                        value={mobileNo}
                        onChange={event => setMobileNo(event.target.value)}
                        required
                    />

                </Form.Group>





            
                <Form.Group className="mb-3" controlId="formBasicEmail">
                    <Form.Label>Email address</Form.Label>

                    <Form.Control 
                        type="email" 
                        placeholder="Enter email" 
                        value={email}
                        onChange = {event => setEmail(event.target.value)}
                        required
                    />

                    <Form.Text className="text-muted">We'll never share your email with anyone else.</Form.Text>
                </Form.Group>

                <Form.Group className="mb-3" controlId="formBasicPassword">
                    <Form.Label>Password</Form.Label>

                    <Form.Control 
                        type="password" 
                        placeholder="Password" 
                        value={password}
                        onChange={event=> setPassword(event.target.value)}
                        required
                    />

                </Form.Group>

                <Form.Group className="mb-3" controlId="formBasicConfirmPassword">
                    <Form.Label>Confirm Password</Form.Label>

                    <Form.Control 
                        type="password" 
                        placeholder="Confirm your Password" 
                        value={confirmPassword}
                        onChange={event => setConfirmPassword(event.target.value)}
                        required
                    />

                </Form.Group>





              

                {btnIsActive ? <Button variant="primary" type="submit">Submit</Button> : <Button variant="danger" disabled type="submit">Submit</Button>}
                
            </Form>
        </>
    )
}