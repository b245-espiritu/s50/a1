


import { useEffect, useState } from "react"
import Course from "../components/course"




export default function Courses(){

    const [courses, setCourses] =useState([]);

useEffect( ()=>{
    fetch(`${process.env.REACT_APP_API_URL}/course/allActiveCourses`)
    .then(result => result.json())
    .then(data => setCourses(data))

},[])

const coursesPrint = courses.map(item=> <Course key={item._id} {...item} />)
    return(
        
        <>
            {coursesPrint}
        </>
      
    )
}