import { Container, Row} from "react-bootstrap";
import { Link } from "react-router-dom";


export default function PageNotFound(){
    return(
        <Container className="col-8 mx-auto mt-5 text-center bg-bs-body-bg">
            <Row>
            <h1>
                Page Not Found
            </h1>

            <span>Go back to the <Link to ="/">Homepage</Link> </span> 
         
            
            </Row>
        </Container>
    )
}