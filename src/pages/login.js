import { useEffect, useState, useContext } from 'react';
import Button from 'react-bootstrap/Button';
import Form from 'react-bootstrap/Form';
import { Navigate, useNavigate } from 'react-router-dom';

import UserContext from '../userContext';
import Swal from 'sweetalert2';

export default function Login(){

    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    const [btnActive, setBtnActive] = useState(true)




    // const [user, setUser] = useState(localStorage.getItem("email"))
    const { user, setUser} = useContext(UserContext);

    const navigate = useNavigate();
    

    useEffect( ()=>{
            if(email !== "" && password !== ""){
                setBtnActive(false)
            }
            else{
                setBtnActive(true)
            }
        }, [email, password])


    function login(event){

        fetch(`${process.env.REACT_APP_API_URL}/user/login`, 
            {
                method: 'POST',
                headers: {
                    'Content-Type' : 'application/json'
                },
                body: JSON.stringify(
                    {
                        email: email,
                        password: password
                    }
                )
            }
        ).then(result => result.json())
        .then(data => {
            if(!data){
                Swal.fire({
                    title: "Authentication failed!",
                    icon: 'error',
                    text : 'Please try again'
                })
            }
            else{
                localStorage.setItem('token', data.auth );
                retrieveUsreDetails(localStorage.getItem('token'));


                Swal.fire({
                    title: "Authentication successfull!",
                    icon: 'success',
                    text: "Welcome to Zuitt!"

                })

                navigate ("/");
            }
        })
       

        const retrieveUsreDetails = (token)=>{
            fetch(`${process.env.REACT_APP_API_URL}/user/details`,
                {
                    headers:{
                        Authorization: `Bearer ${token}`
                    }
                }
            )
            .then(result => result.json())
            .then(data => {
                console.log(data);

                setUser(
                    {
                        id: data._id,
                        isAdmin: data.isAdmin
                    }
                )
            })
        }
        

        

        event.preventDefault();
        // alert('Successfully log-in');
        // localStorage.setItem("email", email);
        // setUser(localStorage.getItem("email"))
        setEmail('');
        setPassword('');
    }  


    return(
     
        
        user? <Navigate to ="*"/>:
            <>
            <h1 className='mt-5 text-center'>Login</h1>
            <Form className='col-12 col-md-6 mx-auto' onSubmit={(event)=> login(event)}>
                <Form.Group className="mb-3" controlId="formBasicEmail">
                    <Form.Label>Email address</Form.Label>

                    <Form.Control 
                        type="email" 
                        placeholder="Enter email" 
                        value={email}
                        onChange = {event => setEmail(event.target.value)}
                        required

                    />

                    <Form.Text className="text-muted">We'll never share your email with anyone else.</Form.Text>
                </Form.Group>


                <Form.Group className="mb-3" controlId="formBasicPassword">
                    <Form.Label>Password</Form.Label>

                    <Form.Control 
                        type="password" 
                        placeholder="Password"
                        value={password}
                        onChange={event=> setPassword(event.target.value)}
                        required
                        
                    />
                </Form.Group>

                
                <Button disabled={btnActive} variant="primary" type="submit">Log-in</Button>
            </Form>
            </>
    )
}


