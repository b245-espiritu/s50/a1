
import Card from 'react-bootstrap/Card';
import { Container, Row, Col } from 'react-bootstrap';

function Hightlights(){
    return(
       <Container className='mt-5'>
            <Row>
                <Col className='col-md-4 col-10 mx-auto m-md-0 m-1' >
                    {/* first card */}
                    <Card className='cardHighlight'>
                        <Card.Body>
                            <Card.Title>Learn from home</Card.Title>
                            <Card.Text>
                            Some quick example text to build on the card title and make up the
                            bulk of the card's content.
                            </Card.Text>
                        </Card.Body>
                    </Card>
                </Col>

                <Col className='col-md-4 col-10 mx-auto m-md-0 m-1' >
                    {/* second Card */}
                    <Card className='cardHighlight'>
                        <Card.Body>
                            <Card.Title>Strudy now pay later</Card.Title>
                            <Card.Text>
                            Some quick example text to build on the card title and make up the
                            bulk of the card's content.
                            </Card.Text>
                        </Card.Body>
                    </Card>
                </Col>


                <Col className='col-md-4 col-10 mx-auto m-md-0 m-1' >
                    {/* third card */}
                    <Card className='cardHighlight'>
                        <Card.Body>
                            <Card.Title>Be part of our community</Card.Title>
                            <Card.Text>
                            Some quick example text to build on the card title and make up the
                            bulk of the card's content.
                            </Card.Text>
                        </Card.Body>
                    </Card>
                </Col>
            </Row>

            
       </Container>
    )
}



export default Hightlights;


