import { useContext, useEffect, useState } from 'react';
import Card from 'react-bootstrap/Card';
import {  Button} from 'react-bootstrap';

import { Container } from "react-bootstrap"
import UserContext from '../userContext';
import { Link } from 'react-router-dom';

function Course(props){

    const {user} = useContext(UserContext);

    const { _id, name, description, price} = props;

    const [enrolledCount, seEnrolledCount] = useState(0);
    const [seatCount, setSeatCount] = useState(30);
    const [btnDisabled, setbtnDisabled] = useState(false)

    const addEnrolledCount = ()=>{
            if(seatCount > 0){
                seEnrolledCount(previous => previous + 1)
                setSeatCount(previous => previous - 1)
            }
            if(seatCount === 1){
                alert('Congratulation for enrolling. You are the last one!')
            }
            
    }
   

    useEffect( ()=>{
        if(seatCount === 0){
            setbtnDisabled(true)
        }
    },[seatCount])


    
    return(
       
          
        <Container className="mt-5">
               
                   <Card >
                       <Card.Body>
                           <Card.Title>{name}</Card.Title>
                           <Card.Subtitle>Description</Card.Subtitle>
                           <Card.Text>{description}</Card.Text>

                           <Card.Subtitle>Price</Card.Subtitle>
                           <Card.Text>{price}</Card.Text>

                           <Card.Subtitle>Current Enrolled</Card.Subtitle>
                           <Card.Text>{enrolledCount}</Card.Text>

                           <Card.Subtitle>Available Seat</Card.Subtitle>
                           <Card.Text>{seatCount}</Card.Text>
                          
                          {
                            user ? <Button as = {Link} to = {`/courseView/${_id}`} disabled={btnDisabled}   variant="primary">See More Details</Button>
                            : <Button as = {Link} to = "/login" variant="primary">Log in</Button>
                          }
                           
                       </Card.Body>
                   </Card>
              
           
        </Container>
                

                
        
    )
}

export default Course;