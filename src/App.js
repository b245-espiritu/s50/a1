import { Fragment,  useEffect,  useState } from 'react';
import './App.css';
import AppNavBar from './components/appNavbar';
import Home from './pages/home';
import Courses from './pages/courses';

import { Container } from 'react-bootstrap';
import Registration from './pages/register';
import Login from './pages/login';

import { BrowserRouter as Router, Routes, Route } from 'react-router-dom';
import Logout from './pages/logout';
import PageNotFound from './pages/pageNotFound';

import CourseView from './components/courseView';

// for useContext
import { UserProvider } from './userContext';

function App() {

  const [user, setUser] = useState(null);

  // useEffect( ()=>{
  //   console.log(user);
  // },[user])

  const unSetUser = ()=>{
    localStorage.removeItem("token");

  }

  useEffect( ()=>{
 
      fetch(`${process.env.REACT_APP_API_URL}/user/details`,
          {
              headers:{
                  Authorization: `Bearer ${localStorage.getItem('token')}`
              }
          }
      )
      .then(result => result.json())
      .then(data => {
       

          if(localStorage.getItem('token') !== null){
            setUser(
              {
                  id: data._id,
                  isAdmin: data.isAdmin
              }
            )
          }
          else{
            setUser(null)
          }
          
      })
  
  
  },[])

  return (
      <UserProvider value={{user, setUser, unSetUser}}>
        <Fragment>

            <Router>
                <AppNavBar/>
                  <Container>
                    <Routes>
                      <Route path='/' element = {<Home/>}/>
                      <Route path='/courses' element={<Courses/>}/>
                      <Route path='/login' element ={<Login/>}/>
                      <Route path='/registration' element={<Registration/>}/>
                      <Route path='/logout' element = {<Logout/>}/>
                      <Route path='*' element = {<PageNotFound/>} />
                      <Route path='/courseView/:courseId' element={<CourseView/>}/>
                    </Routes>
                </Container>
            </Router>
            
                
        </Fragment>
      </UserProvider>  
  );
}

export default App;
